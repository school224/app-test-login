import 'package:app_test_login/model/login_user_response.dart';

class LoginUserResult {
  LoginUserResponse data;
  bool isSuccess;
  int code;
  String msg;

  LoginUserResult(this.data, this.isSuccess, this.code, this.msg);

  factory LoginUserResult.fromJson(Map<String, dynamic> json) {
    return LoginUserResult(
        LoginUserResponse.fromJson(json['data']),
        json['isSuccess'],
        json['code'],
        json['msg']
    );
  }
}