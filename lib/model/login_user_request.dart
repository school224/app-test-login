class LoginUserRequest {
  String username;
  String password;

  LoginUserRequest(this.username, this.password);

  Map<String, dynamic> toJson() {
    //request는 toJson으로 변환해야한다. 객체는 메모리상에서만 존재하기때문에 json으로 바꿔줘야해. json의 자료형은 Map
    final Map<String, dynamic> data = Map<String, dynamic>();

    data['username'] = this.username;
    data['password'] = this.password;

    return data;
  }
}