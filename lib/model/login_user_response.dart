class LoginUserResponse {
  int memberId;

  LoginUserResponse(this.memberId);

  factory LoginUserResponse.fromJson(Map<String, dynamic> json) {
    return LoginUserResponse(
      json['memberId']
    );
  }
}