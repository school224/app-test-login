import 'package:app_test_login/login_check.dart';
import 'package:app_test_login/pages/page_index.dart';
import 'package:app_test_login/pages/page_login.dart';
import 'package:app_test_login/pages/page_mypage.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      localizationsDelegates: [
        GlobalWidgetsLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      builder: BotToastInit(),
      navigatorObservers: [
        BotToastNavigatorObserver(),
      ],
      supportedLocales: [Locale('en', 'US')],
      theme: ThemeData(
        fontFamily: 'GmarketSans',
        primarySwatch: Colors.grey,
      ),

      // 여기에 page_index 가 들어가버리면 무조건 비회원용 메인으로 가기 때문에
      // token 값을 검사하여 token이 있으면 회원용 메인, 없으면 비회원용 메인으로 가게끔 해야 함.
      home: const LoginCheck(),
    );
  }
}
