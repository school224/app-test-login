import 'package:app_test_login/components/component_appbar_actions.dart';
import 'package:app_test_login/components/component_appbar_normal.dart';
import 'package:app_test_login/pages/page_login.dart';
import 'package:flutter/material.dart';

class PageIndex extends StatelessWidget {
  const PageIndex({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarActions(
        title: '비회원용 메인',
      ),
      body: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            padding: EdgeInsets.all(10),
            margin: EdgeInsets.all(50),
            alignment: Alignment.center,
            decoration: BoxDecoration(
              border: Border.all(
                width: 3,
                color: Colors.black
              ),
            ),
            child: Text('접근권한이 없습니다. \n 로그인해주세요.',
            style: TextStyle(
              fontSize: 20
            ),),
          ),
          OutlinedButton(onPressed: () {
            Navigator.push(context, MaterialPageRoute(builder: (context) => PageLogin()));
          }, child: Text('로그인')),
        ],
      ),
    );
  }
}
