import 'package:app_test_login/components/component_appbar_actions.dart';
import 'package:app_test_login/components/component_appbar_normal.dart';
import 'package:app_test_login/config/config_color.dart';
import 'package:app_test_login/functions/token_lib.dart';
import 'package:flutter/material.dart';

class PageMypage extends StatelessWidget {
  PageMypage({Key? key}) : super(key: key);

  Future<void> _logout(BuildContext context) async {
    // functions/token_lib.dart 에서 만든 로그아웃 기능 호출
    TokenLib.logout(context);
  }

  Widget bannerSection = Container(
    decoration: BoxDecoration(
      border: Border.all(
          width: 20,
          color: colorSecondary
      ),
    ),
    padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
    margin: EdgeInsets.symmetric(vertical: 10, horizontal: 250),
    child: Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Image.asset('assets/kbg.png',
          width: 200,
          height: 200,
        fit: BoxFit.cover,),
      ],
    ),
  );

  Widget titleSection = Container(
    padding: const EdgeInsets.all(32),
    child: Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Text(
          '로그인에 성공했어요!',
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 30,
          ),
        ),
      ],
    ),
  );

  Widget buttonSection = Container(
    padding: const EdgeInsets.all(32),
    child: Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        OutlinedButton(
            onPressed: () {},
            child: Text('상품구매'),
        ),
        OutlinedButton(
            onPressed: () {},
            child: Text('나의주문')
        ),
      ],
    ),
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarActions(
        title: '회원용 메인',
      ),
      body: ListView(
        children: [
          bannerSection,
          SizedBox(height: 15,),
          titleSection,
          SizedBox(height: 15,),
          buttonSection,
          OutlinedButton(onPressed: () {
            _logout(context);
          }, child: Text('로그아웃')),
        ],
      ),
    );
  }
}



