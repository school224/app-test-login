import 'package:app_test_login/config/config_api.dart';
import 'package:app_test_login/model/login_user_request.dart';
import 'package:app_test_login/model/login_user_result.dart';
import 'package:dio/dio.dart';

class RepoMember {
  Future<LoginUserResult> doLogin(LoginUserRequest request) async {
    const String baseUrl = '$apiUri/member/login';
    Dio dio = Dio();

    final response = await dio.post(
        baseUrl,
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return LoginUserResult.fromJson(response.data);
  }

}