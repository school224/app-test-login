import 'package:app_test_login/components/component_notification.dart';
import 'package:app_test_login/pages/page_index.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

/*
SharedPreferences 에 데이터가 저장되는 방식은 Map임을 반드시 숙지하고 갈 것.
Map이 무엇인지 알아야 함.
 */
class TokenLib {
  // token 가져오기
  static Future<String?> getToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('token');
  }

  // token 세팅
  static void setToken(String token) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('token', token);
  }

  // 로그아웃 처리
  static void logout(BuildContext context) async {
    // 기존에 닫혀있던 토스트팝업, 커스텀로딩 다 닫고
    BotToast.closeAllLoading();

    // 메모리에 접근해서 key 가 token인 값을 빈문자열('')로 바꿔버리고
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('token', '');

    // 토스트메시지 띄우고
    ComponentNotification(
      success: false,
      title: '로그아웃',
      subTitle: '로그아웃되어 비회원용 메인페이지로 이동합니다.',
    ).call();

    // 지금까지 이동했던 모든 페이지 내역들 싹 다 지우고 page_index(비회원용 메인)으로 강제로 이동시킨다.
    Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (BuildContext context) => const PageIndex()), (route) => false);
  }
}
