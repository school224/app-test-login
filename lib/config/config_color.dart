import 'package:flutter/material.dart';

/*
앱에서 쓸 색상을 정리해 둔 config.
여기서 정의해 둔 색상들로 써야 앱 색상 통일성을 유지 할 수 있음.
primary : 대표 컬러 (네이버로 따지면 초록색)
secondary : 두번째 강조 컬러
빨간색과 회색도 config로 정해 둔 이유는 회색도 종류가 너무 많아 config로 정해두지 않으면
페이지마다 다른 회색이 들어갈 수 있기 때문.
 */
const Color colorPrimary = Color.fromARGB(255, 246, 195, 247);
const Color colorSecondary = Color.fromARGB(255, 173, 217, 240);
const Color colorRed = Color.fromARGB(255, 216, 34, 34);
const Color colorGray = Color.fromRGBO(0, 0, 0, 0.35);
const Color colorDarkGray = Color.fromRGBO(0, 0, 0, 0.65);
const Color colorLightGray = Color.fromRGBO(0, 0, 0, 0.1);
