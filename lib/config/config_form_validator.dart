const String formErrorRequired = '이 값은 필수입니다.';
const String formErrorNumeric = '숫자만 입력해주세요.';
const String formErrorEmail = '올바른 이메일이 아닙니다.';
const String formErrorEqualPassword = '비밀번호가 일치하지 않습니다.';
String formErrorMinNumber(int num) => '$num 이상 입력해주세요.';
String formErrorMaxNumber(int num) => '$num 이하로 입력해주세요.';
String formErrorMinLength(int num) => '$num자 이상 입력해주세요.';
String formErrorMaxLength(int num) => '$num자 이하로 입력해주세요.';


//String fromErrorTest(int testValue) {
//  String result = testValue.toString() + '자 이하로 입력해주세요.';
//  return result;
//}

//String fromErrorTest(int testValue) {
//  String result = testValue.toString() + '자 이하로 입력해주세요.';
//  return testValue.toString() + '자 이하로 입력해주세요.';
//}

//String fromErrorTest(int testValue) {
//  return '$testValue자 이하로 입력해주세요.';
//}