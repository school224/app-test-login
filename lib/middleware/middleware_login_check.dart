import 'package:app_test_login/functions/token_lib.dart';
import 'package:app_test_login/pages/page_index.dart';
import 'package:app_test_login/pages/page_mypage.dart';
import 'package:flutter/material.dart';

class MiddlewareLoginCheck {
  void check(BuildContext context) async {
    String? token = await TokenLib.getToken();

    /*
    최초에 앱을 실행하면 token은 null이고
    한번이라도 로그아웃을 했다면 빈문자열임. (로그아웃 시 setToken에서 ''로 삭제해버림)
    token이 없으면 비회원용 메인으로 강제이동
    token이 있으면 회원용 메인으로 강제이동
     */
    if (token == null || token == ''){
      Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (BuildContext context) => const PageIndex()), (route) => false);
    } else {
      Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (BuildContext context) => PageMypage()), (route) => false);
    }
  }
}